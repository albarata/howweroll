@isTest
public class TestDataFactory {

    /**
        Description: Crear productos de prueba
    */
    public static List<Product2> createProducts(String name, String sku, Integer numProducts){

        List<Product2> products = new List<Product2>();
        
        for(Integer i=0;i<numProducts;i++) {
            Product2 product = new Product2(Name=name);
            product.Cost__c = Integer.valueOf(Math.random());
            product.Warehouse_SKU__c = sku;
            product.Replacement_Part__c = true;
            product.Lifespan_Months__c = 12;
            product.Maintenance_Cycle__c = 2;
            products.add(product);
        }
        insert products;

        return products;

    }

    /**
        Description: Crear vehículos de prueba
    */
    public static List<Vehicle__c> createVehicles(String vehicleName, Integer numVehicles){

        List<Vehicle__c> vehicles = new List<Vehicle__c>();
        
        for(Integer i=0;i<numVehicles;i++) {
            Vehicle__c v = new Vehicle__c(Name=vehicleName + i);
            vehicles.add(v);
        }
        insert vehicles;

        return vehicles;
    }
    
    /**
        Description: Crear workparts de prueba
    */
    public static List<Work_Part__c> createWorkParts(Case maintenanceRequest, List<Product2> equipements){
        List<Work_Part__c> parts = new List<Work_Part__c>();
        
        for(Product2 equipement : equipements){
            Work_Part__c part = new Work_Part__c();
            part.Quantity__c = Integer.valueOf(Math.random());
            part.Maintenance_Request__c = maintenanceRequest.Id;
            part.Equipment__c = equipement.Id;
            
            parts.add(part);            
        }
        
        return parts;
    }
    /**
        Descripcion: Crear maintenace request
     */
    public static Case createMaintenanceRequest(Vehicle__c vehicle, String requestType){
        Case request = new Case();
        request.Vehicle__c = vehicle.id;
        request.Type = requestType;		       
        
        return request;
    }
    
}