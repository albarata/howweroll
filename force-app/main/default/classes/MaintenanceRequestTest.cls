@isTest
public class MaintenanceRequestTest {
    
    @isTest static  void testCreateRepairRequest() {  
        Test.startTest();
        
        List<Vehicle__c> vehicles = TestDataFactory.createVehicles('Vehicle', 100);
		List<Case> requests = new List<Case>();
        
        Set<Id> vehicleIds = new Set<Id>();
        
        //Create requests
        for(Vehicle__c vehicle : vehicles){           
            Case maintenanceRequest = TestDataFactory.createMaintenanceRequest(vehicle, 'Repair');
            requests.add(maintenanceRequest);            
            vehicleIds.add(vehicle.id);
        }
        
        insert requests;
		
		List<Case> results = [SELECT Id, CaseNumber FROM Case 
                              WHERE Vehicle__c in :vehicleIds];
        
        System.assertEquals(vehicleIds.size(), results.size());        
         
        Test.stopTest();        
    }

    @isTest static  void testCreateAndCloseRepairRequest() {                     
        Test.startTest();
        
        List<Vehicle__c> vehicles = TestDataFactory.createVehicles('Vehicle', 100);     
		List<Case> requests = new List<Case>();        
        Set<Id> vehicleIds = new Set<Id>();      
        
        
        //Create requests
        for(Vehicle__c vehicle: vehicles){           
            Case maintenanceRequest = TestDataFactory.createMaintenanceRequest(vehicle, 'Repair');
            maintenanceRequest.Status = 'Closed';
            requests.add(maintenanceRequest);            
            vehicleIds.add(vehicle.id);
        }
        
        insert requests;
		
		List<Case> results = [SELECT Id, CaseNumber FROM Case WHERE Vehicle__c in :vehicleIds];
        
        System.assertEquals(vehicleIds.size(), results.size());        
        
        Test.stopTest();
    }        
    
    @isTest static  void testUpdateMaintenanceRequest() {
        Test.startTest();
        
        List<Product2> products = TestDataFactory.createProducts('Product', '9999',2);
        List<Vehicle__c> vehicles = TestDataFactory.createVehicles('Vehicle', 1);
        List<Work_Part__c> parts = new List<Work_Part__c>();
		List<Case> requests = new List<Case>();
        
        Set<Id> vehicleIds = new Set<Id>();              
                
        // Create requests
        for(Vehicle__c vehicle: vehicles){           
            Case maintenanceRequest = TestDataFactory.createMaintenanceRequest(vehicle, 'Routine Maintenance');             
            requests.add(maintenanceRequest);            
            vehicleIds.add(vehicle.id);
        }
        
        insert requests;
		
		List<Case> results = [SELECT Id, CaseNumber FROM Case WHERE Vehicle__c in :vehicleIds];
        
        System.assertEquals(vehicleIds.size(), results.size()); 
        
        // Update requests
        for(Case maintenanceRequest : requests){
            parts.addAll(TestDataFactory.createWorkParts(maintenanceRequest, products));           
            maintenanceRequest.Status = 'Closed';
        }  
        
        insert parts;
        update requests;
        
        results = [SELECT Id, CaseNumber FROM Case WHERE Vehicle__c in :vehicleIds];
        System.assertEquals(vehicleIds.size()*2, results.size());
        
        Test.stopTest();        
    }    
}